﻿
using ParcialDosIvan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParcialDosIvan
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BuscarPersonas : ContentPage
	{
		public BuscarPersonas ()
		{
			InitializeComponent ();
		}
        public void EliminarPersonas()
        {
            if ((String.IsNullOrEmpty(EntryCedula.Text)))
            {
                DisplayAlert("Aviso", "No se Encuentra Datos", "Volver");
            }
            else
            {
                double b = Convert.ToDouble(EntryCedula.Text);
                // conexion a la base de datos
                using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
                {
                    connection.Table<DatosPersonales>().Delete((i => i.IdentificationCard.Equals(b)));
                    DisplayAlert("Aviso", "Los Datos Han Sido Eliminados", "Ok");
                    Navigation.PushAsync(new MainPage());
                }
            }
        }
        public void BuscarPersona() {

            double Identification = Convert.ToDouble(EntryCedula.Text);

            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd)) {

                var listPer = connection.Table<DatosPersonales>();
                string n = null;
                foreach (var aux in listPer) {

                    if(aux.IdentificationCard == Identification) {

                    LabelName.Text = aux.Name.ToString();
                    LabelLastName.Text = aux.LastName.ToString();
                    LabelDate.Text = aux.Birt.ToString();
                    n = "encontrado";
                    }
                }
                if (n == null)
                {
                    DisplayAlert("Aviso", "No Se Encuentra Registrado", "OK");
                }
           
            }

        }
       

    }
}