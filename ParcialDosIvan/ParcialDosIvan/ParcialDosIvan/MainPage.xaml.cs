﻿
using ParcialDosIvan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ParcialDosIvan
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async private void CreatePersons(object sender, EventArgs e)
        {
            

            if ((string.IsNullOrEmpty(EntryIdentification.Text)) || (string.IsNullOrEmpty(EntryName.Text))
                  ||( string.IsNullOrEmpty(EntryLastName.Text)) )
            {
                await DisplayAlert("Error", "Debe Completar Los Campos", "Ok");
            }
            else
            {
                DatosPersonales tarea = new DatosPersonales()
                {
                    IdentificationCard = Convert.ToDouble(EntryIdentification.Text),
                    Name = EntryName.Text,
                    LastName = EntryLastName.Text,
                    Birt = dateFinish.Date
                };
                // esta linea permite hacer la conexion a la base de datos.

                using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))

                {
                    // crear una tabla en base de datos.
                    connection.CreateTable<DatosPersonales>();
                    // crear registro  en la tabla "tarea"
                    var result = connection.Insert(tarea);
                    if (result > 0)

                    {
                        await Navigation.PushAsync(new LsitarPersonas());
                        await DisplayAlert("Correcto", "La Tarea Se Creo Correctamente", "OK");
                    }
                    else
                    {
                        await DisplayAlert("Error", "La Tarea No Fue Creada", "ok");
                    }

                    EntryIdentification.Text = "";
                    EntryName.Text = "";
                    EntryLastName.Text = "";

                }
              
            }
        }

        async private void BuscarPersonas(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new BuscarPersonas());
        }
    }
}

