﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParcialDosIvan.Models
{
    class DatosPersonales
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }
        // el nombre no mayor de 150 digitos
        public double IdentificationCard { get; set; }

        [MaxLength(150)]
        public string Name { get; set; }

        [MaxLength(150)]
        public string LastName { get; set; }

        public DateTime Birt { get; set; }

    }
}
