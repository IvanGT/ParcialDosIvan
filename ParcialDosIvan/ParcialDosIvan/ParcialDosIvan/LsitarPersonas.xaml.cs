﻿using ParcialDosIvan.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ParcialDosIvan
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LsitarPersonas : ContentPage
	{
		public LsitarPersonas ()
		{
			InitializeComponent ();
		}
        protected override void OnAppearing()
        {
            base.OnAppearing();
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                List<DatosPersonales> listaPersonas;
                listaPersonas = connection.Table<DatosPersonales>().ToList();

                listViewTask.ItemsSource = listaPersonas;
            }
        }

    }
}